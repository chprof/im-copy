var caret = '<span class="caret"></span>';
var caret_white = '<span class="caret caret_white"></span>';
var complicatedTabsCarousel = {
	autoplay: false,
	items: 1,
	margin: 15,
	loop: false,
	nav: true,
	navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
	dots: true,
	dotsContainer: '.complicated-tabs-menu-dots',
	responsive: {
	}
};
function classToggle(elem, className) {
	var element = $(elem);
	element.on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass(className);
	})
}
var select = function() {
	$('.select-outer-tabs').niceSelect();
	$('.select-outer-tabs ul').addClass('list-clr').removeClass('list').wrap('<div class="select__scroll list"></div>')
	$('.select-outer-tabs .select__scroll').mCustomScrollbar();

	$('.select-in-tabs').niceSelect();
	$('.select-in-tabs ul').addClass('list-clr').removeClass('list').wrap('<div class="select__scroll list"></div>')
	$('.select-in-tabs .select__scroll').mCustomScrollbar();

}
var headNavInnerListDetect = function() {
	if ( $('.main-menu li').children('ul').length ) {
		$('.main-menu li').children('ul').closest('li').addClass('hasList').children('div').append(caret_white);
	}
};
var headNavToggleFade = function() {
	$('.main-menu .caret').on('click', function() {
		if ( $(window).width() > 992 ) {
			$(this).addClass('active').closest('div').siblings('ul').removeAttr('style');
			$(this).addClass('active').closest('div').siblings('ul').toggleClass('visible').parent().siblings('.hasList').children('ul').removeClass('visible');
		} else {
			$(this).addClass('active').closest('div').siblings('ul').slideToggle('fast').parent().siblings('.hasList').children('ul').hide('fast');
		}
	})
};
var toggle = function() {
	$('.caret_toggle').on('click', function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		// $(this).parent().siblings().fadeToggle('fast', 'linear');
		$(this).parent().siblings().toggleClass('visible');
	})
};
var hamburgerDecor = function() {
	$('.hamburger').click(function() {
		$(this).toggleClass('is-active');
	})
};
var menuOpenAndClose = function() {
	$('.header__menu-open').click(function() {
		$('.header__menu').addClass('show');
	})
	$('.main-menu__close').click(function() {
		$('.header__menu').removeClass('show');
	})
};
var headElemsReplace = function() {
	if ( $(window).width() < 992 ) {
		$('.other-actions').appendTo('.main-menu__inner');
		$('.header__inner-center').prependTo('.main-menu__inner');
	} else {
		$('.main-menu__inner .other-actions').insertBefore('.header__inner .main-menu');
		$('.main-menu__inner .header__inner-center').insertBefore('.header__inner .other-actions');
	}
};

var navIntroElems = function() {
	$('.nav-intro__description').mCustomScrollbar();
};
var engineeringCarousel = function() {
	var opts = {
		nav: true,
		loop: true,
		autoplay: true,
		autoplayHoverPause: true,
		navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
		dots: false,
		responsive: {
			0: {
				items: 1,
				margin: 0
			},
			576: {
				items: 2,
				margin: 10
			},
			992: {
				items: 3,
				margin: 20
			},
			1200: {
				items: 4,
				margin: 30
			}
		}
	}
	var owl = $('.engineering__carousel').owlCarousel(opts);
}
var equipCarousel = function() {
	var opts = {
		nav: true,
		autoplay: true,
		autoplayHoverPause: true,
		navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
		dots: false,
		responsive: {
			0: {
				items: 1,
				nav: true,
				autoplay: true,
				autoplayHoverPause: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
				dots: false
			},
			576: {
				items: 2,
				nav: true,
				autoplay: true,
				autoplayHoverPause: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
				dots: false
			},
			992: {
				items: 3,
				nav: true,
				autoplay: true,
				autoplayHoverPause: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
				dots: false
			},
			1200: {
				items: 5,
				nav: true,
				autoplay: true,
				autoplayHoverPause: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
				dots: false
			}
		}
	}
	var owl = $('.equip-carousel').owlCarousel(opts);
};
var manufacturesCarousel = function() {
	var opts = {
		nav: false,
		loop: true,
		autoplay: true,
		autoplayHoverPause: true,
		navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
		dots: false,
		responsive: {
			0: {
				items: 1,
				margin: 0
			},
			576: {
				items: 3,
				margin: 30
			},
			992: {
				items: 5,
				margin: 10
			},
			1530: {
				items: 7,
				margin: 50
			}
		}
	}
	var owl = $('.manufactures-carousel').owlCarousel(opts);
};
var caseCarousel = function() {
	var opts = {
		nav: false,
		loop: true,
		autoplay: true,
		autoplayHoverPause: true,
		dots: true,
		responsive: {
			0: {
				items: 1,
				margin: 15
			},
			576: {
				items: 2,
				margin: 15
			},
			992: {
				items: 2,
				margin: 30
			},
			1530: {
				items: 3,
				margin: 30
			}
		}
	}
	$('.case-carousel').owlCarousel(opts);
}
var engineerCarousel = function() {
	var opts = {
		responsive: {
			0: {
				loop: true,
				autoplay: true,
				autoplayHoverPause: true,
				items: 1,
				margin: 15,
				nav: false,
				dots: false,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
			},
			576: {
				loop: true,
				autoplay: true,
				autoplayHoverPause: true,
				items: 2,
				margin: 15,
				nav: true,
				dots: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
			},
			768: {
				loop: true,
				autoplay: true,
				autoplayHoverPause: true,
				items: 3,
				margin: 15,
				nav: true,
				dots: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
			},
			992: {
				loop: true,
				autoplay: true,
				autoplayHoverPause: true,
				items: 3,
				margin: 15,
				nav: true,
				dots: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
			},
			1200: {
				loop: true,
				autoplay: true,
				autoplayHoverPause: true,
				items: 3,
				margin: 15,
				nav: true,
				dots: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
			},
			1530: {
				loop: true,
				autoplay: true,
				autoplayHoverPause: true,
				items: 4,
				margin: 20,
				nav: true,
				dots: true,
				navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>']
			},
		}
	}
	$('.engineer-carousel').owlCarousel(opts);
};
var blueprintsOpts = {
	nav: true,
	loop: true,
	autoplay: true,
	autoplayHoverPause: true,
	dots: false,
	navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
	responsive: {
		0: {
			items: 1,
			margin: 15
		},
		576: {
			items: 2,
			margin: 15
		},
		992: {
			items: 2,
			margin: 20
		},
		1530: {
			items: 3,
			margin: 20
		}
	}
};
var blueprintsList = {
	nav: true,
	loop: true,
	autoplay: true,
	autoplayHoverPause: true,
	dots: true,
	navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
	responsive: {
		0: {
			items: 1,
			margin: 30
		},
		576: {
			items: 2,
			margin: 30
		},
		992: {
			items: 2,
			margin: 30
		},
		1530: {
			items: 3,
			margin: 30
		}
	}
}
var ExhibitionsCarousel = {
	nav: false,
	loop: true,
	autoplay: true,
	autoplayHoverPause: true,
	dots: true,
	navText: ['<span class="caret caret_slider_left"></span>','<span class="caret caret_slider_right"></span>'],
	responsive: {
		0: {
			items: 1,
			margin: 30
		},
		576: {
			items: 2,
			margin: 30
		},
		992: {
			items: 2,
			margin: 30
		},
		1530: {
			items: 3,
			margin: 30
		}
	}
}
var engineerAboutListToggle = function() {
	$('.engineer-about__more').click(function() {
		$(this).text('Скрыть').parent().toggleClass('active');
		$(this).siblings('ul').children().show();
		if ( !$(this).parent().hasClass('active') ) {
			$(this).siblings('ul').children('li:nth-child(n+3)').hide();
			$(this).text('Ещё')
			if ( $(this).siblings('.engineer-about__cities').length ) {
				$(this).text('Ещё 38 городов')
			}
		}
	})
};
var thumbSlider = function() {
	var wrap = $('.edu-diploms-wrap'),
		owl = wrap.find('.owl-carousel');
		dotOuter = wrap.find('.edu-diploms-dots');
	owl.owlCarousel({
		loop:true,
		items:1,
		margin:0,
		nav: false,
		dotsContainer: '.edu-diploms-dots',
		stagePadding: 0,
		autoplay:false,
		responsive: {
			0: {
				items: 1
			}
		}
	});
	
	dotcount = 1;
	
	dotOuter.find('.owl-dot').each(function() {
		$( this ).addClass( 'dotnumber' + dotcount);
		$( this ).attr('data-info', dotcount);
		dotcount=dotcount+1;
	});
	
	slidecount = 1;
	
	owl.find('.owl-item').not('.cloned').each(function() {
		$( this ).addClass( 'slidenumber' + slidecount);
		slidecount=slidecount+1;
	});
	
	dotOuter.find('.owl-dot').each(function() {
		grab = $(this).data('info');
		slidegrab = $('.slidenumber'+ grab +' img').attr('src');
		slidegrabTxt = $('.slidenumber'+ grab +' span');
		$(this).append('<img src="'+ slidegrab + '">');
		$(this).append(slidegrabTxt);
	});
	
	amount = $('.owl-dot').length;
	gotowidth = 100/amount;
}

var uploadFile = function() {
	var elem = $('.input-file'),
		parent = elem.parent(),
		list = parent.siblings();
		attrName = elem.attr('name'),
		nameIndex = 1,
		attrNamePos = attrName.lastIndexOf('['),
		attrNewName = attrName.slice(0, attrNamePos);
		var upload_files_div = $('<div class="upload-files__file-list"></div>');
		var filesWrap = parent.append(upload_files_div);

	elem.on('change', function(e) {
		var files = this.files;
		for ( var i = 0; i < files.length; i++ ) {
			var input = $('<input type="file" class="input-file">'),
				preview = $('<div></div>'),
				btn = $('<button type="button"></button>');
				name = files[i].name,
				size = files[i].size;
				namePos = name.lastIndexOf('.') + 1,
				nameClass = name.slice(namePos);

			upload_files_div.append(input);
			input.files = files[i];
			input.attr('name', attrNewName + '[' + nameIndex + ']');

			list.append(preview);
			if ( size > (1024 * 1024) ) {
				preview.text(name + ' ' + '(' + ( size / (1024 * 1024) ).toFixed(1) + ' мб' + ')');
			} else {
				preview.text(name + ' ' + '(' + (size / 1024).toFixed(1) + ' кб' + ')');
			}
			// preview.text(name + ' ' + size);
			preview.addClass(nameClass);
			preview.append(btn);
			preview.attr('data-index', nameIndex);

			nameIndex++;
			console.log(input.files)
		}
		e.target.value = '';
	})

	$(document).on('click', '.upload-files__list button', function() {
		var currentIndex = $(this).parent().attr('data-index');
		removeFile(currentIndex);
		$(this).parent().remove();
		nameIndex--;
		var parents = $('.upload-files__list div');
		parents.each(function(i, e) {
			$(this).attr('data-index', i+1);
		});
		var filesInput = $('.upload-files__file-list input');
		filesInput.each(function(i, e) {
			var indexUp = i + 1;
			$(this).attr('name', attrNewName + '[' + indexUp + ']' );
		})
	});
	function removeFile(index) {
		var input = parent.find('input'),
		inputCurrent = parent.find( $('input[name="' + attrNewName + '[' + index + ']"') );
		inputCurrent.remove();
	}
};

var tabs = function(elem, contentID) {
	var info, content, contentH;
	$(document).on('click', elem, function(){
		if ( $(this).closest('.complicated-tabs-carousel').length ) {
			content = $(this).closest('.complicated-tabs').find('.complicated-tabs-content');
			contentH = content.outerHeight();
			content.height(contentH);
			$(this).closest('.complicated-tabs').children('.complicated-tabs-content').children('.loader').fadeIn();
		} else {
			content = $(this).parent().parent().siblings();
			contentH = content.outerHeight();
			content.height(contentH);
			$(this).parent().parent().siblings().children('.loader').fadeIn();

		}

		$(elem).removeClass('active');
		$(this).addClass('active');
		info = $(this).attr("href");
		if ( content.find('.nav-sidebar').length ) {
			$(".sidebar-stick").trigger("sticky_kit:detach");
		}
		$(contentID).fadeOut(loadContent);
		return false;			
	})
	function loadContent(){
		$(contentID).load(info, "", function(){
			$(contentID).fadeIn(function() {
				var that = $(this);
				function toggleClassTab(elem, className) {
					var element = $(elem);
					element.on('click', function(e) {
						e.preventDefault();
						$(this).toggleClass(className);
					})
				};
				hideLoader();
				content.height('auto');
				$('.nav-intro__description').mCustomScrollbar();
				equipCarousel();
				manufacturesCarousel();
				engineerCarousel();
				engineeringCarousel();
				$('.blueprintsList-carousel').owlCarousel(blueprintsList);
				$('.blueprints-carousel').owlCarousel(blueprintsOpts);
				$('.exhibitions-carousel').owlCarousel(ExhibitionsCarousel);
				$('.complicated-tabs-carousel').owlCarousel(complicatedTabsCarousel);
				thumbSlider();
				toggle();
				caseCarousel();
				toggleClassTab( that.find('.article-head__caret'), 'active' );
				toggleClassTab( that.find('.favourite-button'), 'active' );
				if ( that.find('.nav-sidebar').length ) {
					$('.sidebar-stick').stick_in_parent({
						offset_top: 0
					});

				}

				$('[data-toggle="tooltip"]').tooltip('update');
				if ( !$(this).parent().siblings('.equipment-tabs').length ) {
					$('.select-in-tabs').niceSelect();
					$('.select-in-tabs ul').addClass('list-clr').removeClass('list').wrap('<div class="select__scroll list"></div>')
					$('.select-in-tabs .select__scroll').mCustomScrollbar();
				}
			})
		})
	}
	function hideLoader(){
		if ( $(elem).closest('.complicated-tabs').length ) {
			if ( $(elem).closest('.site-tabs-embed').length ) {
				$(elem).parent().parent().siblings().children('.loader').fadeOut();				
			} else {
				$('.complicated-tabs').children('.complicated-tabs-content').children('.loader').fadeOut();
			}

		} else {
			$(elem).parent().parent().siblings().children('.loader').fadeOut();			

		}
	}
};
$(document).ready(function() {
	select();
	headNavInnerListDetect();
	headNavToggleFade();
	toggle();
	hamburgerDecor();
	menuOpenAndClose();
	headElemsReplace();
	navIntroElems();
	engineeringCarousel();
	equipCarousel();
	manufacturesCarousel();
	engineerCarousel();
	thumbSlider();
	if ( $('.upload-files').length ) {
		uploadFile();
	}
	$('.complicated-tabs-carousel').owlCarousel(complicatedTabsCarousel);
	// fileUpload();
	$('[data-toggle="tooltip"]').tooltip();
	if ( $(window).width() > 767 ) {
		$('[data-toggle="tooltip"]').tooltip('enable');
	} else {
		$('[data-toggle="tooltip"]').tooltip('disable');
	}
	$('.blueprintsList-carousel').owlCarousel(blueprintsList);
	$('.blueprints-carousel').owlCarousel(blueprintsOpts);
	$('.exhibitions-carousel').owlCarousel(ExhibitionsCarousel);
	engineerAboutListToggle();
	caseCarousel();
	tabs('.site-tabs a', '#content');
	tabs('.site-tabs-embed a', '#contentIn');
	tabs('.equipment-tabs a', '#contentInner');
	tabs('.nav-intro__list a', '#contentInner');
	tabs('.complicated-tabs-carousel a', '#complicatedContentIn');
	classToggle('.article-head__caret', 'active');
	classToggle('.favourite-button', 'active');
	if ( $('.nst-init').length ) {
		$('.nst-init').nstSlider({
			"left_grip_selector": ".leftGrip",
			"value_bar_selector": ".bar",
			"value_changed_callback": function(cause, leftValue, rightValue) {
				var $container = $(this).parent();
				$container.find('.leftLabel').text(leftValue);
			}
		});
		var nstMax = $('.nstSlider').attr('data-range_max');
		var nstStepVal = 0;
		var nstValDiff = 3;
		var nstW = $('.nstSlider').width();
		var nstStep = $('.nstSlider').attr('data-step');
		var stepLength = (nstMax / nstStep) + 1;
		var stepLeftVal = nstW / stepLength;
		var stepLeftDiff = 0;
		for (var i = 0; i < stepLength; i++ ) {
			var step = $('<div class="nstSlider__step">' + nstStepVal + '</div>');
			nstStepVal = nstStepVal + nstValDiff;
			$('.nstSlider').append(step);
		};
	}
	$('.sticky').stick_in_parent({
		offset_top: 30
	});
	$('.sidebar-stick').stick_in_parent({
		offset_top: 0
	});
	$('.catalog-call').click(function() {
		$('.page__col_catalog_mob').toggleClass('active');
		
	})
	$('body').scrollspy({target: ".nav-sidebar-spy"})
	$(document).on('click', ".nav-sidebar a", function(event) {
	  if (this.hash !== "") {
		event.preventDefault();
		var hash = this.hash;
		$('html, body').animate({
		  scrollTop: $(hash).offset().top
		}, 500, function(){
		  window.location.hash = hash;
		});
	  }

	});

	$(window).on('resize', function() {
		var that = $(this);
		var waitForFinalEvent = (function () {
		  var timers = {};
		  return function (callback, ms, uniqueId) {
			if (!uniqueId) {
			  uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
			  clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		  };
		})();
		waitForFinalEvent(function() {
			headElemsReplace();
			if ( $(window).width() > 767 ) {
				$('[data-toggle="tooltip"]').tooltip('enable');
				$('.sticky').stick_in_parent({
					offset_top: 30
				});
			} else {
				$('[data-toggle="tooltip"]').tooltip('disable');
				// $('.sticky').stick_in_parent('destroy');
				$(".sticky").trigger("sticky_kit:detach");
			}
		}, 500, "resize")
	})
})

